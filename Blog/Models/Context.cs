﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using Blog.Models;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace Blog.Models
{
    public class Context : DbContext
    {
        public DbSet<Bloger> Blogeri { get; set; }
        public DbSet<Zapis> Zapisi { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

        }
    }
}