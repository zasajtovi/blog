﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Blog.Models
{
    public class Zapis
    {
        public int zapisID { get; set; }
        public string naslov { get; set; }
        public string tekst { get; set; }
        public virtual Bloger Bloger { get; set; }
        public int BlogerID { get; set; }
    }
}