﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using Blog.Models;

namespace Blog.Models
{
    public class Initializer : DropCreateDatabaseIfModelChanges<Context>
    {
         protected override void Seed(Context context)
        {
            var blogeri = new List<Bloger>
            {
                new Bloger { ime = "Igor", admin=true, lozinka="igor", mail="igor@mail.com"  },
                new Bloger { ime = "Ivana", admin=false, lozinka="ivana", mail="ivana@mail.com" },
                new Bloger { ime = "Stefan", admin=false, lozinka="stefan", mail="stefan@mail.com" },
            };
             blogeri.ForEach(s => context.Blogeri.Add(s));
             context.SaveChanges();

             var zapisi = new List<Zapis>
             {
                 new Zapis { naslov="Zevs", tekst="Vo grckata mitologija Zevs e kral na bogovite, vladetel na planinata Olimp i bog na neboto i gromot", BlogerID=1 },
                 new Zapis { naslov="Rim", tekst="Rim e grad i posebna opstina vo Italija. Rim e glaven grad na Italija", BlogerID=2 },
                 new Zapis { naslov="Pariz", tekst="Pariz e glaven grad na Francija, a voedno pretstavuva i administrativen, politicki, stopanski, kulturen i obrazoven centar. Pariz e najposetuvaniot grad vo svetot.", BlogerID=2 },
                 new Zapis { naslov="Venera", tekst="Venera e vtora po blizina planeta do Sonceto i orbitira na sekoi 224 zemjini dena. Po mesecinata e vtoriot najsjaen objekt, koj moze da se vidi noke.", BlogerID=3 },
                 new Zapis { naslov="Mars", tekst="Mars e cetrvta planeta od Sonceto so prosecna oddalecenost od 227.940.000 km (1,52 AU). Taa e narecena i crvenata planeta.", BlogerID=3 },

             };
             zapisi.ForEach(s=> context.Zapisi.Add(s));
             context.SaveChanges();

         }
    }
}