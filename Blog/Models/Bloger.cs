﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Blog.Models
{
    public class Bloger
    {
        public int BlogerID { get; set; }
        public string ime { get; set; }
        public string mail { get; set; }
        public string lozinka { get; set; }
        public bool admin { get; set; }
        public virtual ICollection<Zapis> Zapisi { get; set; }
    }
}