﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Blog.Models;

namespace Blog.Controllers
{ 
    public class KontrolerController : Controller
    {
        private Context db = new Context();

        public ActionResult Najava()
        {
            Bloger bloger = new Bloger();
            return View(bloger);
        }

        [HttpPost]
        public ActionResult Najava(Bloger b)
        {
            if (ModelState.IsValid)
            {
                var korisnici =
                from m in db.Blogeri
                where b.ime == m.ime && b.lozinka == m.lozinka
                select m;
                Bloger bl = korisnici.SingleOrDefault<Bloger>();
                Session["BlogerID"] = bl.BlogerID;
                return RedirectToAction("Personalna", "Kontroler", new { id = (int)Session["BlogerID"] });
            }
            return View(b);

        }

        public ActionResult Registracija()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Registracija(Bloger bloger)
        {
            if (ModelState.IsValid)
            {
                bloger.admin = false;
                db.Blogeri.Add(bloger);
                db.SaveChanges();
                Session["BlogerID"] = bloger.BlogerID;
                return RedirectToAction("Personalna", "Kontroler", new { id = (int)Session["BlogerID"] });
            }

            return View(bloger);
        }

        public ActionResult Dodadi()
        {
            return View();
        }
        
        [HttpPost]
        public ActionResult Dodadi(Zapis zapis)
        {
            if (ModelState.IsValid)
            {
                zapis.BlogerID = (int)Session["BlogerID"];
                db.Zapisi.Add(zapis);
                db.SaveChanges();
                return RedirectToAction("Personalna", "Kontroler", new { id = (int)Session["BlogerID"] });
               

            }
            return View(zapis);
        }

        public ActionResult Personalna(int id)
        {
            
            ViewBag.id = id.ToString();
            IEnumerable<Zapis> query =
            from m in db.Zapisi
            where m.BlogerID == id
            select m;
            return View(query);
            
        }

        public ActionResult Site()
        {
            IEnumerable<Zapis> query =
            from m in db.Zapisi
            select m;
            return View(query);
        }

        //
        // GET: /Kontroler/

        public ViewResult Index()
        {
            return View(db.Blogeri.ToList());
        }

      
        //
        // GET: /Kontroler/Delete/5
 
        public ActionResult Delete(int id)
        {
            Bloger bloger = db.Blogeri.Find(id);
            return View(bloger);
        }

        //
        // POST: /Kontroler/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {            
            Bloger bloger = db.Blogeri.Find(id);
            db.Blogeri.Remove(bloger);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult IzbrisiZapis()
        {
            int id = (int)Session["BlogerID"];
            Bloger bloger = db.Blogeri.Find(id);
            ICollection<Zapis> zapisi = db.Zapisi.ToList<Zapis>();
            IList<Zapis> z = new List<Zapis>();
            foreach (Zapis item in zapisi)
            {
                if (item.BlogerID == id) z.Add(item);

            }
            return View(z); 
        }

        public ActionResult Izbrisi(int id)
        {
            Zapis zapis = db.Zapisi.Find(id);
            return View(zapis);
        }

        [HttpPost, ActionName("Izbrisi")]
        public ActionResult Potvrdno(int id)
        {
            Zapis zapis = db.Zapisi.Find(id);
            db.Zapisi.Remove(zapis);
            db.SaveChanges();
            return RedirectToAction("Personalna", "Kontroler", new { id = (int)Session["BlogerID"] });
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}